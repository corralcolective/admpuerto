<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function mes_texto($mes='1',$idioma='es'){
	//echo "-----".$mes."idio".$idioma;
	$mes=(int)$mes;
	$aMes=array('es'=>array(1=>"Enero",
						2=>"Febrero",
						3=>"Marzo",
						4=>"Abril",
						5=>"Mayo",
						6=>"Junio",
						7=>"Julio",
						8=>"Agosto",
						9=>"Septiembre",
						10=>"Octubre",
						11=>"Noviembre",
						12=>"Diciembre",
			),'en'=>array(1=>"January",
						2=>"February",
						3=>"March",
						4=>"April",
						5=>"May",
						6=>"June",
						7=>"July",
						8=>"August",
						9=>"September",
						10=>"October",
						11=>"November",
						12=>"December")
		);
	return $aMes[$idioma][$mes];
}
function mes_texto_corto($mes='1',$idioma='es'){
	$mes=(int)$mes;
	$aMes=array('es'=>array(1=>"Ene",
						2=>"Feb",
						3=>"Mar",
						4=>"Abr",
						5=>"May",
						6=>"Jun",
						7=>"Jul",
						8=>"Ago",
						9=>"Sep",
						10=>"Oct",
						11=>"Nov",
						12=>"Dic",
			),'en'=>array(1=>"Jan",
						2=>"Feb",
						3=>"Mar",
						4=>"Apr",
						5=>"May",
						6=>"Jun",
						7=>"Jul",
						8=>"Aug",
						9=>"Sep",
						10=>"Oct",
						11=>"Nov",
						12=>"Dec")
		);
return $aMes[$idioma][$mes];
}

function dia_texto($dia='0',$idioma='es'){
		$aDia=array('es'=>array(
						1=>"Lunes",
						2=>"Martes",
						3=>"Miércoles",
						4=>"Jueves",
						5=>"Viernes",
						6=>"Sabado",
						7=>"Domingo"),
					'en'=>array(					   
					    1=>'Monday',
					    2=>'Tuesday',
					    3=>'Wednesday',
					    4=>'Thursday',
					    5=>'Friday',
					    6=>'Saturday',
					    7=>'Sunday')
					);
	//	echo '******'.$dia;
		return $aDia[$idioma][$dia];
}