<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Int extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
            parent::__construct();
            $this->load->database();
            $this->load->helper(array('funciones_fecha'));
            // Your own constructor code
    }

	public function index()
	{
		
		$this->load->view('welcome_message');
	}
	function rip_tags($string) {
   
    // ----- remove HTML TAGs -----
    $string = preg_replace ('/<[^>]*>/', ' ', $string);
   
    // ----- remove control characters -----
    $string = str_replace("\r", '', $string);    // --- replace with empty space
    $string = str_replace("\n", ' ', $string);   // --- replace with space
    $string = str_replace("\t", ' ', $string);   // --- replace with space
   
    // ----- remove multiple spaces -----
    $string = trim(preg_replace('/ {2,}/', ' ', $string));
   
    return $string;

	}
	public function get_noticia(){
		
		if(false!=$this->input->get('uid')){
			$uidNoticia=$this->input->get('uid');
		}else{
			$uidNoticia=0;
		}

		if($uidNoticia>0){
		//	$this->load->helper();
			$this->db->where('ui', $uidNoticia);
			$query= $this->db->get('noticias');
			//echo $this->db->last_query();
			if($query->num_rows==1){
				if($oRegistro=$query->result() ){
					$oInfo=json_decode($oRegistro[0]->contenido);
					$aImagenes=(array)$oInfo->images;
					foreach ($aImagenes as $key => $value) {
						$imagen['src']=$key;
						$imagen['width']=$value->width;
						$imagen['height']=$value->height;
						break;
					}
					//$propiedades_imagen=get_image_properties($imagen,TRUE);
					$aDatos = array('operacion' => true,
							'uid' =>$oRegistro[0]->ui,
							'desc'=>html_entity_decode($oInfo->desc, ENT_COMPAT, 'UTF-8'),
							'title'=>$oInfo->title,
							'imagen'=>$imagen['src'],
							'imagen_width'=>$imagen['width'],
							'imagen_height'=>$imagen['height']
			 		);
				}

			}else{
			$aDatos = array('operacion' => false,
							'registro' =>0
			 );
			}
				
		}else{
			$aDatos = array('operacion' => false,
							'registro' =>0
			 );
			
		}
		echo json_encode($aDatos);
	}
	public function get_noticias(){
 		$query= $this->db->get('noticias');
 			if($query->num_rows==0){
 				$Datos=array('registos'=>0,
 							'mensaje'=>'No existen registros');
 			}else{
 				$registros=array();
 				$registro=array();
				foreach ($query->result() as $registroDB)
				{
				 	$contenido=json_decode($registroDB->contenido);
				     $registro['title']=$contenido->title;
				     $registro['link']=$contenido->link;
				     $registro['date_text']=$contenido->date_text;
				     $registro['year']=$contenido->year;
				      $registro['thumbnail']=$contenido->thumbnail;
				      $registro['date_ui']=$contenido->date_ui;
				     $registros[]=$registro;
				}
 				$Datos=array('num_registros'=>$query->num_rows,
 							'mensaje'=>'Si existen registros',
 							'registros'=>$registros);
 			}
 			echo json_encode($Datos);
	}
	public function ins_noticias(){
		$doc = new DOMDocument();
		$this->load->helper(array('img_propierties'));
		  $doc->load('http://puertomanzanillo.com.mx/espi/rss');
		  $arrFeeds = array();
			$this->db->empty_table('noticias'); 
		  foreach ($doc->getElementsByTagName('item') as $node) {
		  	$html=$node->getElementsByTagName('description')->item(0)->nodeValue;
		  	preg_match_all('/<img[^>]+>/i',$html, $aImagenes); 
	 		 $imgArray = array();
		  	foreach( $aImagenes[0] as $img_tag)
			{
				preg_match( '@src="([^"]+)"@' , $img_tag, $match );
				$src = array_pop($match);
				preg_match( '@alt="([^"]+)"@' , $img_tag, $match );
				$alt = array_pop($match);
				$html=$this->rip_tags(strip_tags(str_replace($img_tag, '', $html)));

				if(trim($src)!=''):
				$propiedades_imagen=get_image_properties($src,TRUE);
				$imgArray[$src]['src']=$src;
    			$imgArray[$src]['alt']=$alt;
    			$imgArray[$src]['width']=$propiedades_imagen['width'];
    			$imgArray[$src]['height']=$propiedades_imagen['height'];
    			endif;
			} 
			 $pubDate=$node->getElementsByTagName('pubDate')->item(0)->nodeValue;
			 $date_ui=date("YmdHis", strtotime($pubDate));
		    $nocitia = array ( 
		      'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
		      'desc' => $html,
		      'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
		      'date' => $pubDate,
		      'date_text' => date("d", strtotime($pubDate)).' '.mes_texto_corto(date("m", strtotime($pubDate))),
		      'year'=>date("Y", strtotime($pubDate)),
		      'thumbnail' =>'https://cdn1.iconfinder.com/data/icons/nuvola2/48x48/actions/thumbnail.png',
		      'date_ui' => $date_ui,
		      'images'=> $imgArray
		      );
		  $datos=array('ui'=>$date_ui,
		  					'contenido'=>json_encode($nocitia)
		  	);
		 $this->db->where('ui', $date_ui);
		 $query= $this->db->get('noticias');
		if($query->num_rows==1){
			 $this->db->where('ui', $date_ui);
			$this->db->update('noticias',$datos);
		}else{
			$this->db->insert('noticias',$datos);
		}
		// 

		  //  array_push($arrFeeds, $itemRSS);
		  }
		  //echo json_encode($arrFeeds);

	}
	public function arribos_zarpes(){
		$llaves=array("codigo","trafico","buque","tipo_buque","viaje","linea_naviera","consignatoria","procedencia","pdestino","bandera","tllamada","eslora","tbr","fcmn","calado","fcrucelp","ffondeo","mfondeo","fviradoancla","fpilotoa","piloto","remolcadores","fcescolleras","fplinea","ftatracado","lancha","tramo","costadoa","fechainicio","fechatermino","fpilotoa2","piloto2","remolcadores2","librecabos","lancha2","fcescolleras2","fdpiloto","ffondeo2","mfondeo2","fviradoancla2","caladoz","fcrucelp2","atracado2","enpuerto");

		$this->load->helper('dom');
		$url = "http://puertomanzanillo.com.mx/esps/2110490/arribos-y-zarpes";
		 $html = file_get_html($url);
 
		// find all link on Codeigniter Site
		// var_dump($html->find('table[class=tablaConsulta]'));
		// echo "<pre>";
		 /*foreach($html->find('td') as $element) 
   			echo $element->plaintext. '<br>';*/
   		$aBuques=array();
		foreach($html->find('table[class=tablaConsulta]') as $e){
			if(isset($e->nodes)) {
				foreach($e->nodes[3] as $valor){
					//echo "---><br>";
					if(is_array($valor))
					foreach($valor as $uno){
					//echo "<br>########<br>";
					//var_dump($uno);
					//die();
					$cuenta=0;
					if(is_object($uno)){
						$Buque=array();
						foreach($uno->children as $ele){
							//print_r($ele);
							$Buque[$llaves[$cuenta]]=trim($ele->plaintext);
							$cuenta++;
						}
						//echo "----------------->>>>>>>><br>";
						
						if(count($Buque)>0)
						$aBuques[]=$Buque;
					}
					}
				}
				
				//echo "---------------<br>";
			}
			
		}
		echo "<pre>";
		//echo "todos";
		print_r($aBuques);
	}
	public function buques_programados(){
		$llaves=array('codigo','fprogramacion','farribo','trafico','buque','tipobuque','viaje','lineanaviera','aconsignataria','procedencia','destino','maniobrista','operaciones','fondeo','atraque','tramo','bitaproa','bitapopa','costado','fechainicio','fechatermino','fecha_destraque','fecha_fondeo');

		$this->load->helper('dom');
		$url = "http://puertomanzanillo.com.mx/esps/2110488/buques-programados";
		 $html = file_get_html($url);
 
		// find all link on Codeigniter Site
		// var_dump($html->find('table[class=tablaConsulta]'));
		// echo "<pre>";
		 /*foreach($html->find('td') as $element) 
   			echo $element->plaintext. '<br>';*/
   		$aBuques=array();
		foreach($html->find('table[class=tablaConsulta]') as $e){
			if(isset($e->nodes)) {
				foreach($e->nodes[3] as $valor){
					//echo "---><br>";
					if(is_array($valor))
					foreach($valor as $uno){
					//echo "<br>########<br>";
					//var_dump($uno);
					//die();
					$cuenta=0;
					if(is_object($uno)){
						$Buque=array();
						foreach($uno->children as $ele){
							$Buque[$llaves[$cuenta]]=trim($ele->plaintext);
							$cuenta++;
						}
						//echo "----------------->>>>>>>><br>";
						
						if(count($Buque)>0)
						$aBuques[]=$Buque;
					}
					}
				}
				
				//echo "---------------<br>";
			}
			
		}
		echo "<pre>";
		//echo "todos";
		print_r($aBuques);
	}
	public function getClima(){
		if(false!=$this->input->get('lang')){
			$idioma=$this->input->get('lang');
		}else{
				$idioma='es';
		}

		$sentencia="select id,valor,idioma,clave,fecha_act
 from ma_services where clave='clima_".$idioma."' and DATE_ADD(fecha_act, INTERVAL '30' MINUTE)>now()";
		//echo $sentencia;
		 $query = $this->db->query($sentencia);
//echo $query->num_rows();	
	if ($query->num_rows()==0)
	{
		$rowClima=$query->result_array();
		if($idioma=='es'):
			$oClima =  json_decode($this->curl->simple_get(DIRECCION_CLIMAes));
		else:
			$oClima =  json_decode($this->curl->simple_get(DIRECCION_CLIMAen));
		endif;
		$aFecha=explode('-',$oClima->data->weather[0]->date);

		$posicion=strripos($oClima->data->current_condition[0]->weatherIconUrl[0]->value,'/')+1;
		$icono= substr($oClima->data->current_condition[0]->weatherIconUrl[0]->value,$posicion);
		if($idioma=='es'):
			$descripcion=$oClima->data->current_condition[0]->lang_es[0]->value;
			$temp_act=$oClima->data->current_condition[0]->temp_C;
			$maxtemp=$oClima->data->weather[0]->maxtempC;
			$mintemp=$oClima->data->weather[0]->mintempC;
			$velocidad_viento=$oClima->data->current_condition[0]->windspeedKmph;
		else:
			
			$descripcion=$oClima->data->current_condition[0]->weatherDesc[0]->value;
			$temp_act=$oClima->data->current_condition[0]->temp_F;
			$maxtemp=$oClima->data->weather[0]->maxtempF;
			$mintemp=$oClima->data->weather[0]->mintempF;
			$velocidad_viento=$oClima->data->current_condition[0]->windspeedMiles;


			
		endif;
		//var_dump($oClima->data->request[0]->query);
		$Clima = array('localidad'=>$oClima->data->request[0]->query,
						'sensasion_termica'=>$oClima->data->current_condition[0]->FeelsLikeC,
						'temperatura_actual'=>$temp_act,
						'humedad'=>$oClima->data->current_condition[0]->humidity,
						'visibilidad'=>$oClima->data->current_condition[0]->visibility,
						'descripcion'=>$descripcion,
						'presion_atmosferica'=>$oClima->data->current_condition[0]->pressure,
						'velocidad_viento'=>$velocidad_viento,
						'direccion_viento'=>$oClima->data->current_condition[0]->winddirDegree,
						'cardinalida_viento'=>$oClima->data->current_condition[0]->winddir16Point,
						'maxima_temperatura'=>$maxtemp,
						'minima_temperatura'=>$mintemp,
						'indice_uv'=>$oClima->data->weather[0]->uvIndex,
						'amanecer'=>$oClima->data->weather[0]->astronomy[0]->sunrise,
						'atardecer'=>$oClima->data->weather[0]->astronomy[0]->sunset,
						'fecha'=>$oClima->data->weather[0]->date,
						'anio'=>$aFecha[0],
						'mes'=>$aFecha[1],
						'mes_texto'=>mes_texto($aFecha[1],$idioma),
						'dia'=>$aFecha[2],
						'hora'=> time(),
						'icono'=>$icono,
						'dia_texto'=>dia_texto(date('N'),$idioma)
		);
		
		 $datos =array(
               'valor' =>  json_encode($Clima)
            );
		 $this->db->where('clave', 'clima_'.$idioma);
		
			$this->db->update('ma_services',$datos);
		//	echo $this->db->last_query();
			echo json_encode($Clima);

	}else{ //$query->num_rows() == 0)

		$rowClima=$query->result_array();
		 echo ($rowClima[0]['valor']);
	}//$query->num_rows() == 0)

}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */