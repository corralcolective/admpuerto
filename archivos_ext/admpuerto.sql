/*
Navicat MySQL Data Transfer

Source Server         : .
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : admpuerto

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2014-12-16 05:16:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ma_services`
-- ----------------------------
DROP TABLE IF EXISTS `ma_services`;
CREATE TABLE `ma_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor` varchar(800) DEFAULT NULL,
  `fecha_act` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `idioma` varchar(2) DEFAULT NULL,
  `clave` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ma_services
-- ----------------------------
INSERT INTO `ma_services` VALUES ('1', '{\"sensasion_termica\":\"26\",\"temperatura_actual\":\"24\",\"humedad\":\"83\",\"visibilidad\":\"16\",\"descripcion\":\"Despejado\",\"presion_atmosferica\":\"1012\",\"velocidad_viento\":\"9\",\"direccion_viento\":\"90\",\"cardinalida_viento\":\"E\",\"maxima_temperatura\":\"31\",\"minima_temperatura\":\"21\",\"indice_uv\":\"7\",\"hora\":1418711884}', '2014-12-16 00:38:04', 'es', 'clima_es');
INSERT INTO `ma_services` VALUES ('2', 'cdcdcdcs', '2014-12-15 23:19:57', 'en', 'clima_en');
